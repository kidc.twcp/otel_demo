import opentelemetry from '@opentelemetry/api'
import { WebTracerProvider } from '@opentelemetry/sdk-trace-web'
import { Resource } from '@opentelemetry/resources'
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions'
import { ConsoleSpanExporter, SimpleSpanProcessor, BatchSpanProcessor, } from '@opentelemetry/sdk-trace-base'
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-http'
import { ZoneContextManager } from '@opentelemetry/context-zone'

import { registerInstrumentations } from '@opentelemetry/instrumentation'

// 1. create a provider
const provider = new WebTracerProvider({
    resource: new Resource({
        [SemanticResourceAttributes.SERVICE_NAME]: 'react_1', // assign service name
    })
})
// 如果是Node.js後端使用，將provider換成NodeTracerProvider

// 2. create and regist an exporter
const exporter = new OTLPTraceExporter({
    url: 'http://192.168.1.31:4318/v1/traces',
})
// Span processor有兩個可以選，SimpleSpanProcessor與BatchSpanProcessor，Simple在Span.end()時會馬上傳送，Batch則是排程或span量到一定程度時傳送
// 如建議使用Batch，會有較好的效能
provider.addSpanProcessor(new BatchSpanProcessor(exporter))
// @opentelemetry/exporter-jaeger 不支援 Browser，只能用在Node.js後端使用
// 可以一次指定多個exporter
//provider.addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter()))

// 3. (optional) Changing default contextManager to use ZoneContextManager - supports asynchronous operations
provider.register({
    contextManager: new ZoneContextManager(),
})

// 4. (optional) Registrying instruments
// current support fetch instrument
// axios is not supported now, use custom injection indeed, see InjectHeader function
/*registerInstrumentations({
    instrumentations: [
        ,
    ]
})*/

// get tracer do what you want to trace
const tracer = opentelemetry.trace.getTracer('tracer')
// AKA: const tracer = provider.getTracer('tracer') // input string並沒有很重要，前面並沒有塞tracer name，這邊可以隨意填，不清楚這邊的設計

export const StartSpan = (spanName: string, spanOption: any = undefined, currentSpan: any = undefined) => {
    if (currentSpan) {
        const currentCtx = opentelemetry.context.active()
        const ctx = opentelemetry.trace.setSpan(currentCtx, currentSpan)
        return tracer.startSpan(spanName, spanOption, ctx)
    }
    return tracer.startSpan(spanName, spanOption)
}

export const InjectHeader = (span: any, header: any = undefined) => {
    if (span && span._spanContext)
        return {
            ...header,
            'traceparent': `00-${span._spanContext.traceId}-${span._spanContext.spanId}-${String(span._spanContext.traceFlags).padStart(2, '0')}`
        }
    return header ?? {}
}
