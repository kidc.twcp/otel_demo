package example;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.TracerProvider;
import io.opentelemetry.api.common.Attributes;

import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.SpanExporter;
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor;

import io.opentelemetry.exporter.logging.LoggingSpanExporter;

import io.opentelemetry.semconv.resource.attributes.ResourceAttributes;

public final class Java1 {
        private static final String SERVICE_NAME = "Java_1";

        public static void main(String[] args) throws InterruptedException {
                Resource serviceNameResource = Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, SERVICE_NAME));
                SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
                        .addSpanProcessor(SimpleSpanProcessor.create(new LoggingSpanExporter()))
                        .setResource(Resource.getDefault().merge(serviceNameResource))
                        .build();

                OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
                        .setTracerProvider(sdkTracerProvider)
                        .buildAndRegisterGlobal();

                Java1 java1 = new Java1(openTelemetry);
                java1.doWork();
        }

        private final Tracer tracer;

        public Java1(OpenTelemetry openTelemetry) {
                this.tracer = openTelemetry.getTracer("");
        }

        public void doWork() throws InterruptedException {
                Span span = tracer.spanBuilder("function name")
                        .setAttribute("tag.1", "test")
                        .startSpan();
                try {
                        Thread.sleep(500);
                } finally {
                        span.end();
                }
        }
}
