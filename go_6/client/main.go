package main

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/trace/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"

	rpc "example_rpc"
)

func main() {
	exporter, err := jaeger.New(jaeger.WithAgentEndpoint(
		jaeger.WithAgentHost("jaeger"),
		jaeger.WithAgentPort("6831"),
	))
	if err != nil {
		fmt.Print("connect jaeger fail")
		return
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("go_6_client"),
		)),
	)
	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))
	defer tp.Shutdown(context.Background())

	ctx, span := tp.Tracer("").Start(context.Background(), "clientFunction")
	defer span.End()

	propagationInfo := make(map[string][]string)
	otel.GetTextMapPropagator().Inject(ctx, propagation.HeaderCarrier(propagationInfo))
	reqMetadata := make(metadata.MD)
	for key, value := range propagationInfo {
		reqMetadata.Set(key, value...)
	}
	ctx = metadata.NewOutgoingContext(ctx, reqMetadata.Copy())

	conn, err := grpc.DialContext(ctx, "go_6_server:10000", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	client := rpc.NewExampleRPCClient(conn)
	resp, err := client.Hello(ctx, &rpc.Greeting{
		Content: "hello",
	})
	span.SetAttributes(attribute.String("rpc.response", resp.Content))
}
