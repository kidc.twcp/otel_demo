import opentelemetry from '@opentelemetry/api'

const tracer = opentelemetry.trace.getTracer('tracer')

export const ActiveSpan = () => {
    const handleClick = () => {
        tracer.startActiveSpan('active span click', (span) => {
            span.setAttribute('key', 'value')
            funcA()
            span.end()
        })
    }

    const funcA = () => {
        tracer.startActiveSpan('funcA', (span) => {
            subA()
            span.end()
        })
    }

    const subA = () => {
        const span = opentelemetry.trace.getSpan(opentelemetry.context.active())
        console.log(span)
        span?.setAttribute('subkey', 'subvalue')
    }

    return <button onClick={handleClick}>Active span way</button>
}
