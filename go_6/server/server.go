package main

import (
	"context"

	rpc "example_rpc"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/metadata"
)

func CreateServer(tracerProvider *sdktrace.TracerProvider) *Server {
	return &Server{
		tp: tracerProvider,
	}
}

type Server struct {
	tp *sdktrace.TracerProvider
	rpc.UnimplementedExampleRPCServer
}

func (server Server) Hello(ctx context.Context, in *rpc.Greeting) (out *rpc.Greeting, err error) {
	reqMetadata, ok := metadata.FromIncomingContext(ctx)
	if ok {
		ctx = otel.GetTextMapPropagator().Extract(ctx, propagation.HeaderCarrier(reqMetadata))
	}
	span := trace.SpanFromContext(ctx)
	span.SetAttributes(
		attribute.String("client.greeting", in.Content),
	)
	defer span.End()

	out = &rpc.Greeting{
		Content: "hello 2",
	}
	return
}
