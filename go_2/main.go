package main

import (
	"context"
	"fmt"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

func main() {
	exporter, err := stdouttrace.New(
		stdouttrace.WithPrettyPrint(),
	)
	if err != nil {
		panic(err)
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithSpanProcessor(
			sdktrace.NewBatchSpanProcessor(exporter),
		),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("go_2"),
		)),
	)
	otel.SetTracerProvider(tp)
	defer tp.Shutdown(context.Background())

	ctx, span := tp.Tracer("").Start(context.Background(), "function name")
	defer span.End()

	FuncA(ctx)
	time.Sleep(50 * time.Millisecond)
	err = FuncC(ctx)
	if err != nil {
		span.RecordError(err)
	}
}

func FuncA(ctx context.Context) {
	tp := otel.GetTracerProvider()
	ctx, span := tp.Tracer("").Start(ctx, "FuncA")
	defer span.End()

	time.Sleep(500 * time.Millisecond)
	FuncB(ctx)
}

func FuncB(ctx context.Context) {
	tp := otel.GetTracerProvider()
	ctx, span := tp.Tracer("").Start(ctx, "FuncB")
	defer span.End()

	time.Sleep(100 * time.Millisecond)
}

func FuncC(ctx context.Context) error {
	tp := otel.GetTracerProvider()
	ctx, span := tp.Tracer("").Start(ctx, "FuncC")
	defer span.End()

	time.Sleep(300 * time.Microsecond)
	return fmt.Errorf("something goes wrong")
}
