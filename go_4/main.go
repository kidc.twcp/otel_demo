package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/trace/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
)

func main() {
	exporter, err := jaeger.New(jaeger.WithAgentEndpoint(
		jaeger.WithAgentHost("jaeger"),
		jaeger.WithAgentPort("6831"),
	))
	if err != nil {
		fmt.Print("connect jaeger fail")
		return
	}
	tp := sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("go_4"),
		)),
	)
	otel.SetTracerProvider(tp)
	defer tp.Shutdown(context.Background())

	mux := http.NewServeMux()
	mux.Handle("/WeatherForecast", otelhttp.NewHandler(http.HandlerFunc(WeatherForecast), "WeatherForcast"))
	err = http.ListenAndServe(":5000", mux)
	if err != nil {
		fmt.Println("something goes wrong")
	}
}

func WeatherForecast(w http.ResponseWriter, r *http.Request) {
	tp := otel.GetTracerProvider()
	ctx, span := tp.Tracer("").Start(r.Context(), "WeatherForecast")
	defer span.End()

	function(ctx)
	time.Sleep(time.Millisecond * 10)
	w.Write([]byte("response from go"))
}

func function(ctx context.Context) {
	tp := otel.GetTracerProvider()
	tracer := tp.Tracer("")
	_, span := tracer.Start(ctx, "function")
	defer span.End()

	time.Sleep(time.Millisecond * 500)
}
