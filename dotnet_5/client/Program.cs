﻿using System.Diagnostics;

using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using OpenTelemetry;
using OpenTelemetry.Trace;
using OpenTelemetry.Context.Propagation;
using OpenTelemetry.Resources;

public class Program
{
    private static readonly ActivitySource Activity = new ActivitySource(nameof(Main));
    private static readonly TextMapPropagator Propagator = new TraceContextPropagator();
    public static void Main()
    {
        using var tracerProvider = Sdk.CreateTracerProviderBuilder()	// initial global tracer agent
			.SetSampler(new AlwaysOnSampler())
			.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("dotnet_5_client"))
			.AddSource(nameof(Main))		// 如果很多class，全部列入Source內
			.AddJaegerExporter(opts =>
            {
                opts.AgentHost = "jaeger";
                opts.AgentPort = 6831;
            })
			.Build();

        Task.Run(async () =>
        {
            using (var activity = Activity.StartActivity("clientFunction"))
            using (var client = new HttpClient())
            {
                // span id可以inject到其他key-value型態，再依傳輸型態傳到另一個service server
                /*Dictionary<string, string> spanValue = new Dictionary<string, string>{};
                Propagator.Inject(
                    new PropagationContext(activity.Context, Baggage.Current),
                    spanValue,
                    InjectContextIntoDictionary
                );*/
                Propagator.Inject(
                    new PropagationContext(activity.Context, Baggage.Current),
                    client,
                    InjectContextIntoHeader
                );

                var response = await client.GetAsync("http://localhost:5000/WeatherForecast");
                activity.AddTag("http.status", response.StatusCode);
                string responseBody = await response.Content.ReadAsStringAsync();
                activity.AddTag("server.response", responseBody);
            }
        }).Wait();

		tracerProvider.Shutdown();
    }

    private static void InjectContextIntoHeader(HttpClient carrier, string key, string value)
    {
        carrier.DefaultRequestHeaders.Add(key, value);
    }

    private static void InjectContextIntoDictionary(Dictionary<string, string> carrier, string key, string value)
    {
        carrier.Add(key, value);
    }
}
