﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;
using Microsoft.AspNetCore.Http;
using OpenTelemetry.Context.Propagation;

namespace dotnet_5_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private static readonly ActivitySource Activity = new ActivitySource(nameof(WeatherForecastController));
        private static readonly TextMapPropagator Propagator = new TraceContextPropagator();

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            // inject/extract有包在instrument內，如果沒有用instrument，才需要自己處理inject/extract
            // inject/extract在實作上有相同結構，通常會需要同時實作
            // 但如果是使用openTelemetry沒有提供的server instrumentation，就只需要實作server side extracter
            /*using (var activity = new Activity("get"))
            {
                var ctx = Propagator.Extract(
                    default,
                    Request,
                    Extract
                );
                activity.SetParentId(ctx.ActivityContext.TraceId, ctx.ActivityContext.SpanId, ctx.ActivityContext.TraceFlags);
                // do more things...
            }*/

            Function();
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        private void Function()
        {
            using (var activity = Activity.StartActivity("Function"))
            {
                Thread.Sleep(500);
            }
        }

        private IEnumerable<string> Extract(HttpRequest request, string key)
        {
            var value = new Microsoft.Extensions.Primitives.StringValues{};
            request.Headers.TryGetValue(key, out value);
            return value;
        }
    }
}
