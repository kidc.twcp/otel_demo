﻿using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;
using OpenTelemetry.Resources;

public class Program
{
	private static readonly ActivitySource Activity = new ActivitySource(nameof(Main));	//使用tracer的Source

	public static void Main()
	{
		using var tracerProvider = Sdk.CreateTracerProviderBuilder()	// initial global tracer agent
			.SetSampler(new AlwaysOnSampler())
			.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("dotnet_1"))
			.AddSource(nameof(Main))		// 如果很多class，全部列入Source內
			.AddConsoleExporter()		// 設定exporter
			.Build();

		using (var activity = Activity.StartActivity("function name"))
		{
			activity?.AddEvent(new ActivityEvent("event"));
			activity?.AddTag("key", "value");
		}
		
		tracerProvider.Shutdown();
	}
}
