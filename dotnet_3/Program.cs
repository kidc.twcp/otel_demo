﻿using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;
using OpenTelemetry.Resources;
using System.Threading;

public class Program
{
	private static readonly ActivitySource Activity = new ActivitySource(nameof(Main));	//使用tracer的Source

	public static void Main()
	{
		using var tracerProvider = Sdk.CreateTracerProviderBuilder()	// initial global tracer agent
			.SetSampler(new AlwaysOnSampler())
			.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("dotnet_3"))
			.AddSource(nameof(Main))		// 如果很多class，全部列入Source內
			.AddOtlpExporter(opts =>
			{
				opts.Endpoint = new System.Uri("http://otel-collector:55680");
				opts.TimeoutMilliseconds = 1000;
			})
			/*.AddJaegerExporter(opts =>
			{
				opts.AgentHost = "jaeger";
				opts.AgentPort = 6831;
			})*/		// 設定exporter
			.Build();

		using (var activity = Activity.StartActivity("main function"))
		{
			FuncA();
			Thread.Sleep(50);
			FuncC();
		}

		tracerProvider.Shutdown();
	}

	private static void FuncA()
	{
		using (var activity = Activity.StartActivity("FuncA"))
		{
			var value = new string('a', 70000);
			activity.AddTag("hugeTag", value);
			Thread.Sleep(500);
			FuncB();
		}
	}

	private static void FuncB()
	{
		using (var activity = Activity.StartActivity("FuncB"))
		{
			Thread.Sleep(100);
		}
	}

	private static void FuncC()
	{
		using (var activity = Activity.StartActivity("FuncB"))
		{
			Thread.Sleep(300);
		}
	}
}
