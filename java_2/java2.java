package example;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.StatusCode;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.TracerProvider;
import io.opentelemetry.api.common.Attributes;

import io.opentelemetry.context.Scope;

import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.export.SpanExporter;
import io.opentelemetry.sdk.trace.export.SimpleSpanProcessor;

import io.opentelemetry.exporter.logging.LoggingSpanExporter;

import io.opentelemetry.semconv.resource.attributes.ResourceAttributes;

public final class Java2 {
        private static final String SERVICE_NAME = "Java_2";

        public static void main(String[] args) throws InterruptedException {
                Resource serviceNameResource = Resource.create(Attributes.of(ResourceAttributes.SERVICE_NAME, SERVICE_NAME));
                SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
                        .addSpanProcessor(SimpleSpanProcessor.create(new LoggingSpanExporter()))
                        .setResource(Resource.getDefault().merge(serviceNameResource))
                        .build();

                OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
                        .setTracerProvider(sdkTracerProvider)
                        .buildAndRegisterGlobal();

                Java2 java2 = new Java2(openTelemetry);
                java2.funcA();
                Thread.sleep(50);
                java2.funcC();

                sdkTracerProvider.shutdown();
        }

        private final Tracer tracer;

        public Java2(OpenTelemetry openTelemetry) {
                this.tracer = openTelemetry.getTracer("");
        }

        public void funcA() throws InterruptedException {
                Span span = tracer.spanBuilder("func A")
                        .setAttribute("tag.1", "test")
                        .startSpan();
                try (Scope scope = span.makeCurrent()) {
                        Thread.sleep(500);
                        funcB();
                } finally {
                        span.end();
                }
        }

        public void funcB() throws InterruptedException {
                Span span = tracer.spanBuilder("func B")
                        .startSpan();
                try (Scope scope = span.makeCurrent()) {
                        Thread.sleep(100);
                } finally {
                        span.end();
                }
        }

        public void funcC() throws InterruptedException {
                Span span = tracer.spanBuilder("func C")
                        .startSpan();
                try (Scope scope = span.makeCurrent()){
                        Thread.sleep(300);
                        throw new Exception();
                } catch (Exception e) {
                        span.setStatus(StatusCode.ERROR, "error message");
                } finally {
                        span.end();
                }
        }
}
