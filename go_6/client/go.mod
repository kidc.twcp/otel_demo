module go_6_client

go 1.16

replace example_rpc => ../proto/

require (
	example_rpc v0.0.0
	go.opentelemetry.io/otel v1.0.0-RC1
	go.opentelemetry.io/otel/exporters/trace/jaeger v1.0.0-RC1
	go.opentelemetry.io/otel/sdk v1.0.0-RC1
	google.golang.org/grpc v1.39.0
)
