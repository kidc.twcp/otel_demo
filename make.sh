#!/bin/bash

docker pull mcr.microsoft.com/dotnet/sdk:5.0
docker network create log
docker run -d -p 16686:16686 --name jaeger --network log jaegertracing/all-in-one 
docker run -d -p 3000:3000 --name grafana --network log grafana/grafana
cd dotnet_1
make image
cd ../dotnet_2
make image
cd ../dotnet_3
make image
cd ../dotnet_3_1
make image
cd ../dotnet_4
make image
cd ../dotnet_5
make image
cd ..