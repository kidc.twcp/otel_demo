import { Span } from './span'
import { ActiveSpan } from './activeSpan'

function App() {
  return (<>
    <Span />
    <ActiveSpan />
  </>);
}

export default App;
