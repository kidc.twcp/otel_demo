﻿using System;
using System.Diagnostics;
using OpenTelemetry;
using OpenTelemetry.Trace;
using OpenTelemetry.Resources;
using System.Threading;

public class Program
{
	private static readonly ActivitySource Activity = new ActivitySource(nameof(Main));	//使用tracer的Source

	public static void Main()
	{
		using var tracerProvider = Sdk.CreateTracerProviderBuilder()	// initial global tracer agent
			.SetSampler(new AlwaysOnSampler())
			.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("dotnet_2"))
			.AddSource(nameof(Main))		// 如果很多class，全部列入Source內
			.AddConsoleExporter()		// 設定exporter
			.Build();

		using (var activity = Activity.StartActivity("main function"))
		{
			try
			{
				FuncA();
				Thread.Sleep(50);
				FuncC();
			} catch (Exception e)
			{
				activity.RecordException(e);
			}
		}

		tracerProvider.Shutdown();
	}

	private static void FuncA()
	{
		using (var activity = Activity.StartActivity("FuncA"))
		{
			Thread.Sleep(500);
			FuncB();
		}
	}

	private static void FuncB()
	{
		using (var activity = Activity.StartActivity("FuncB"))
		{
			Thread.Sleep(100);
		}
	}

	private static void FuncC()
	{
		using (var activity = Activity.StartActivity("FuncC"))
		{
			Thread.Sleep(300);
			throw new Exception("something goes wrong");
		}
	}
}
