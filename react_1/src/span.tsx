import opentelemetry, { SpanStatusCode } from '@opentelemetry/api'
import axios from 'axios'
import { StartSpan, InjectHeader } from './tracer'

export const Span = () => {
    const handleClick = () => {
        const span = StartSpan('click')
        funcA(span)
        span.end()
    }
    

    const funcA = (parentSpan: any) => {
        const span = StartSpan('funcA', undefined, parentSpan)
        setTimeout(() => { }, 2000)
        callAPI(span)
        span.end()
    }
    
    const callAPI = async (parentSpan: any) => {
        const span = StartSpan('callAPI', undefined, parentSpan)
        try {
            const { data } = await axios.get('http://google.com', {
                headers: InjectHeader(span, {
                    token: '',
                })
            })
            span.setAttribute('response', data)
        } catch (e: any) {
            span.recordException(e)
            span.setStatus({ code: SpanStatusCode.ERROR})
        }
        span.end()
    }

  return <button onClick={handleClick}>span way</button>
}

